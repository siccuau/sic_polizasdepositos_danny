object FPlantillas: TFPlantillas
  Left = 0
  Top = 0
  Caption = 'Plantillas'
  ClientHeight = 487
  ClientWidth = 889
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pcModulos: TPageControl
    Left = 0
    Top = 0
    Width = 889
    Height = 487
    ActivePage = TabSheet4
    Align = alClient
    TabOrder = 0
    object TabSheet3: TTabSheet
      Caption = 'Ventas'
      object pcPlantillasVentas: TPageControl
        Left = 0
        Top = 0
        Width = 881
        Height = 459
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = 'Cr'#233'dito'
          object Label1: TLabel
            Left = 43
            Top = 56
            Width = 65
            Height = 13
            Caption = 'Tipo de P'#243'liza'
          end
          object Label10: TLabel
            Left = 40
            Top = 101
            Width = 137
            Height = 13
            Caption = 'Eliminar Formula (Alt + Supr)'
          end
          object Label11: TLabel
            Left = 40
            Top = 82
            Width = 135
            Height = 13
            Caption = 'Eliminar Cuenta (Ctrl+ Supr)'
          end
          object cbTiposPolizasCredito: TDBLookupComboBox
            Left = 120
            Top = 48
            Width = 145
            Height = 21
            KeyField = 'TIPO_POLIZA_ID'
            ListField = 'NOMBRE'
            ListSource = dsTiposPolizasCredito
            TabOrder = 0
          end
          object grdPlantillaCredito: TDBGrid
            Left = 24
            Top = 120
            Width = 833
            Height = 297
            DataSource = dsPlantillasCredito
            Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            OnDblClick = grdPlantillaCreditoDblClick
            OnKeyDown = grdPlantillaCreditoKeyDown
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Contado'
          ImageIndex = 1
          object Label4: TLabel
            Left = 43
            Top = 56
            Width = 65
            Height = 13
            Caption = 'Tipo de P'#243'liza'
          end
          object Label12: TLabel
            Left = 43
            Top = 81
            Width = 135
            Height = 13
            Caption = 'Eliminar Cuenta (Ctrl+ Supr)'
          end
          object Label13: TLabel
            Left = 43
            Top = 100
            Width = 137
            Height = 13
            Caption = 'Eliminar Formula (Alt + Supr)'
          end
          object cbTiposPolizasContado: TDBLookupComboBox
            Left = 120
            Top = 48
            Width = 145
            Height = 21
            KeyField = 'TIPO_POLIZA_ID'
            ListField = 'NOMBRE'
            ListSource = dsTiposPolizascontado
            TabOrder = 0
          end
          object grdPlantillaContado: TDBGrid
            Left = 24
            Top = 120
            Width = 825
            Height = 289
            DataSource = dsPlantillasContado
            Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            OnDblClick = grdPlantillaContadoDblClick
            OnKeyDown = grdPlantillaContadoKeyDown
          end
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Compras'
      ImageIndex = 1
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 881
        Height = 459
        ActivePage = TabSheet5
        Align = alClient
        TabOrder = 0
        object TabSheet5: TTabSheet
          Caption = 'Cr'#233'dito'
          object Label2: TLabel
            Left = 43
            Top = 56
            Width = 65
            Height = 13
            Caption = 'Tipo de P'#243'liza'
          end
          object Label14: TLabel
            Left = 43
            Top = 101
            Width = 137
            Height = 13
            Caption = 'Eliminar Formula (Alt + Supr)'
          end
          object Label15: TLabel
            Left = 43
            Top = 82
            Width = 135
            Height = 13
            Caption = 'Eliminar Cuenta (Ctrl+ Supr)'
          end
          object cbTiposPolizasCreditoP: TDBLookupComboBox
            Left = 120
            Top = 48
            Width = 145
            Height = 21
            KeyField = 'TIPO_POLIZA_ID'
            ListField = 'NOMBRE'
            ListSource = dsTiposPolizasCreditoP
            TabOrder = 0
          end
          object grdPlantillaCreditoCompras: TDBGrid
            Left = 24
            Top = 120
            Width = 833
            Height = 289
            DataSource = dsPlantillasCreditoCompras
            Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            OnDblClick = grdPlantillaCreditoComprasDblClick
            OnKeyDown = grdPlantillaCreditoComprasKeyDown
          end
        end
        object TabSheet6: TTabSheet
          Caption = 'Contado'
          ImageIndex = 1
          object Label3: TLabel
            Left = 43
            Top = 56
            Width = 65
            Height = 13
            Caption = 'Tipo de P'#243'liza'
          end
          object cbTiposPolizasContadoP: TDBLookupComboBox
            Left = 120
            Top = 48
            Width = 145
            Height = 21
            KeyField = 'TIPO_POLIZA_ID'
            ListField = 'NOMBRE'
            ListSource = dsTiposPolizasContadoP
            TabOrder = 0
          end
          object grdPlantillaContadoCompras: TDBGrid
            Left = 24
            Top = 120
            Width = 846
            Height = 289
            DataSource = dsPlantillasContadoCompras
            Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
          end
        end
      end
    end
    object tsPagosCliente: TTabSheet
      Caption = 'Pagos de Clientes'
      ImageIndex = 2
      object Label5: TLabel
        Left = 51
        Top = 64
        Width = 65
        Height = 13
        Caption = 'Tipo de P'#243'liza'
      end
      object cbTiposPolizasPagosclientes: TDBLookupComboBox
        Left = 128
        Top = 56
        Width = 145
        Height = 21
        KeyField = 'TIPO_POLIZA_ID'
        ListField = 'NOMBRE'
        ListSource = dstiposPolizasPagosClientes
        TabOrder = 0
      end
      object grdPlantillaPagosclientes: TDBGrid
        Left = 32
        Top = 128
        Width = 681
        Height = 289
        DataSource = dsPlantillapagosaclientes
        Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
    object tsPagosProveedor: TTabSheet
      Caption = 'Pagos a proveedores'
      ImageIndex = 3
      object Label6: TLabel
        Left = 51
        Top = 64
        Width = 65
        Height = 13
        Caption = 'Tipo de P'#243'liza'
      end
      object cbTipospolizasPagospro: TDBLookupComboBox
        Left = 128
        Top = 56
        Width = 145
        Height = 21
        KeyField = 'TIPO_POLIZA_ID'
        ListField = 'NOMBRE'
        ListSource = dsTiposPolizasPagosPro
        TabOrder = 0
      end
      object grdPlantillaPagospro: TDBGrid
        Left = 32
        Top = 128
        Width = 681
        Height = 289
        DataSource = dsPlantillapagosaproveedores
        Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
    object tsNotasCreditocxc: TTabSheet
      Caption = 'Notas de Credito CxC'
      ImageIndex = 5
      object Label8: TLabel
        Left = 51
        Top = 64
        Width = 65
        Height = 13
        Caption = 'Tipo de P'#243'liza'
      end
      object cbTiposPolizasNCCXC: TDBLookupComboBox
        Left = 128
        Top = 56
        Width = 145
        Height = 21
        KeyField = 'TIPO_POLIZA_ID'
        ListField = 'NOMBRE'
        ListSource = dsTiposPolizasNCCXC
        TabOrder = 0
      end
      object grdPlantillaNCCXC: TDBGrid
        Left = 32
        Top = 128
        Width = 681
        Height = 289
        DataSource = dsPlantillaNCCXC
        Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
    object tsNotasCreditocxp: TTabSheet
      Caption = 'Notas de Credito CxP'
      ImageIndex = 6
      object Label9: TLabel
        Left = 51
        Top = 64
        Width = 65
        Height = 13
        Caption = 'Tipo de P'#243'liza'
      end
      object cbTiposPolizasNCCXP: TDBLookupComboBox
        Left = 128
        Top = 56
        Width = 145
        Height = 21
        KeyField = 'TIPO_POLIZA_ID'
        ListField = 'NOMBRE'
        ListSource = dsTiposPolizasNCCXP
        TabOrder = 0
      end
      object grdPlantillaNCCXP: TDBGrid
        Left = 32
        Top = 128
        Width = 681
        Height = 289
        DataSource = dsPlantillaNCCXP
        Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
    object tsNomina: TTabSheet
      Caption = 'Nomina'
      ImageIndex = 4
      object Label7: TLabel
        Left = 51
        Top = 64
        Width = 65
        Height = 13
        Caption = 'Tipo de P'#243'liza'
      end
      object cbTiposPolizasNomina: TDBLookupComboBox
        Left = 128
        Top = 56
        Width = 145
        Height = 21
        KeyField = 'TIPO_POLIZA_ID'
        ListField = 'NOMBRE'
        ListSource = dsTiposPolizasNomina
        TabOrder = 0
      end
      object grdPlantillanomina: TDBGrid
        Left = 32
        Top = 128
        Width = 681
        Height = 289
        DataSource = dsPlantillanomina
        Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
  end
  object qryTiposPolizasContado: TFDQuery
    AfterScroll = qryTiposPolizasContadoAfterScroll
    Connection = Conexion
    SQL.Strings = (
      'select * from tipos_polizas order by nombre')
    Left = 616
    Top = 48
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'Password=masterkey'
      'User_Name=sysdba'
      'Database=E:\Microsip Pruebas\VET LOYA PRUEBA 2019.FDB'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 336
    Top = 80
  end
  object dsTiposPolizascontado: TDataSource
    DataSet = qryTiposPolizasContado
    Left = 616
    Top = 96
  end
  object qryPlantillasContado: TFDQuery
    AutoCalcFields = False
    AfterOpen = qryPlantillasContadoAfterOpen
    AfterInsert = qryPlantillasContadoAfterInsert
    BeforeScroll = qryPlantillasContadoBeforeScroll
    IndexesActive = False
    Connection = Conexion
    SQL.Strings = (
      
        'select * from sic_plantillas_co_det where tipo = '#39'C'#39' and clasif ' +
        '= '#39'Venta'#39
      'ORDER BY CREACION')
    Left = 484
    Top = 184
    object qryPlantillasContadoTIPO: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      FixedChar = True
      Size = 1
    end
    object qryPlantillasContadoCTA: TStringField
      DisplayLabel = 'Cuenta'
      DisplayWidth = 24
      FieldName = 'CTA'
      Origin = 'CTA'
      Size = 50
    end
    object qryPlantillasContadoDEBE: TStringField
      DisplayLabel = 'Debe'
      DisplayWidth = 24
      FieldName = 'DEBE'
      Origin = 'DEBE'
      Size = 50
    end
    object qryPlantillasContadoHABER: TStringField
      DisplayLabel = 'Haber'
      DisplayWidth = 24
      FieldName = 'HABER'
      Origin = 'HABER'
      Size = 50
    end
    object qryPlantillasContadoFORMULA_DEBE: TStringField
      DisplayLabel = 'Formula Debe'
      DisplayWidth = 30
      FieldName = 'FORMULA_DEBE'
      Origin = 'FORMULA_DEBE'
      Size = 50
    end
    object qryPlantillasContadoFORMULA_HABER: TStringField
      DisplayLabel = 'Formula Haber'
      DisplayWidth = 30
      FieldName = 'FORMULA_HABER'
      Origin = 'FORMULA_HABER'
      Size = 50
    end
    object qryPlantillasContadoCREACION: TSQLTimeStampField
      FieldName = 'CREACION'
      Origin = 'CREACION'
    end
    object qryPlantillasContadoCLASIF: TStringField
      FieldName = 'CLASIF'
    end
  end
  object dsPlantillasContado: TDataSource
    DataSet = qryPlantillasContado
    Left = 484
    Top = 272
  end
  object qryTiposPolizasCredito: TFDQuery
    AfterScroll = qryTiposPolizasCreditoAfterScroll
    Connection = Conexion
    SQL.Strings = (
      'select * from tipos_polizas order by nombre')
    Left = 408
    Top = 56
  end
  object dsTiposPolizasCredito: TDataSource
    DataSet = qryTiposPolizasCredito
    Left = 408
    Top = 104
  end
  object qryPlantillasCredito: TFDQuery
    AutoCalcFields = False
    AfterOpen = qryPlantillasCreditoAfterOpen
    AfterInsert = qryPlantillasCreditoAfterInsert
    BeforeScroll = qryPlantillasContadoBeforeScroll
    IndexesActive = False
    Connection = Conexion
    SQL.Strings = (
      
        'select * from sic_plantillas_co_det where tipo = '#39'R'#39' and clasif=' +
        #39'Venta'#39
      'ORDER BY CREACION')
    Left = 412
    Top = 184
    object StringField1: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      FixedChar = True
      Size = 1
    end
    object qryPlantillasCreditoCta: TStringField
      DisplayLabel = 'Cuenta'
      DisplayWidth = 24
      FieldName = 'CTA'
      Origin = 'CTA'
      Size = 50
    end
    object StringField3: TStringField
      DisplayLabel = 'Debe'
      DisplayWidth = 24
      FieldName = 'DEBE'
      Origin = 'DEBE'
      Size = 50
    end
    object StringField4: TStringField
      DisplayLabel = 'Haber'
      DisplayWidth = 24
      FieldName = 'HABER'
      Origin = 'HABER'
      Size = 50
    end
    object qryPlantillasCreditoFORMULA_DEBE: TStringField
      DisplayLabel = 'Formula Debe'
      DisplayWidth = 30
      FieldName = 'FORMULA_DEBE'
      Origin = 'FORMULA_DEBE'
      Size = 50
    end
    object qryPlantillasCreditoFORMULA_HABER: TStringField
      DisplayLabel = 'Formula Haber'
      DisplayWidth = 30
      FieldName = 'FORMULA_HABER'
      Origin = 'FORMULA_HABER'
      Size = 50
    end
    object qryPlantillasCreditoCREACION: TSQLTimeStampField
      FieldName = 'CREACION'
      Origin = 'CREACION'
      Visible = False
    end
    object qryPlantillasCreditoCLASIF: TStringField
      FieldName = 'CLASIF'
      Visible = False
    end
  end
  object dsPlantillasCredito: TDataSource
    DataSet = qryPlantillasCredito
    Left = 412
    Top = 272
  end
  object qryTiposPolizasCreditoP: TFDQuery
    AfterScroll = qryTiposPolizasCreditoPAfterScroll
    Connection = Conexion
    SQL.Strings = (
      'select * from tipos_polizas order by nombre')
    Left = 48
    Top = 280
  end
  object dsTiposPolizasCreditoP: TDataSource
    DataSet = qryTiposPolizasCreditoP
    Left = 48
    Top = 328
  end
  object qryTiposPolizasContadoP: TFDQuery
    AfterScroll = qryTiposPolizasContadoPAfterScroll
    Connection = Conexion
    SQL.Strings = (
      'select * from tipos_polizas order by nombre')
    Left = 136
    Top = 280
  end
  object dsTiposPolizasContadoP: TDataSource
    DataSet = qryTiposPolizasContadoP
    Left = 136
    Top = 328
  end
  object qryPlantillasCreditoCompras: TFDQuery
    AutoCalcFields = False
    AfterOpen = qryPlantillasCreditoComprasAfterOpen
    AfterInsert = qryPlantillasCreditoComprasAfterInsert
    BeforeScroll = qryPlantillasContadoBeforeScroll
    IndexesActive = False
    Connection = Conexion
    SQL.Strings = (
      
        'select * from sic_plantillas_co_det where tipo = '#39'R'#39' and clasif ' +
        '= '#39'Compra'#39
      'ORDER BY CREACION')
    Left = 572
    Top = 184
    object StringField5: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      FixedChar = True
      Size = 1
    end
    object qryPlantillasCreditoComprasCTA: TStringField
      DisplayLabel = 'Cuenta'
      DisplayWidth = 24
      FieldName = 'CTA'
      Origin = 'CTA'
      Size = 50
    end
    object StringField7: TStringField
      DisplayLabel = 'Debe'
      DisplayWidth = 24
      FieldName = 'DEBE'
      Origin = 'DEBE'
      Size = 50
    end
    object StringField8: TStringField
      DisplayLabel = 'Haber'
      DisplayWidth = 24
      FieldName = 'HABER'
      Origin = 'HABER'
      Size = 50
    end
    object qryPlantillasCreditoComprasFORMULA_DEBE: TStringField
      DisplayLabel = 'Formula Debe'
      DisplayWidth = 30
      FieldName = 'FORMULA_DEBE'
      Origin = 'FORMULA_DEBE'
      Size = 50
    end
    object qryPlantillasCreditoComprasFORMULA_HABER: TStringField
      DisplayLabel = 'Formula Haber'
      DisplayWidth = 30
      FieldName = 'FORMULA_HABER'
      Origin = 'FORMULA_HABER'
      Size = 50
    end
    object SQLTimeStampField1: TSQLTimeStampField
      FieldName = 'CREACION'
      Origin = 'CREACION'
    end
    object qryPlantillasCreditoComprasCLASIF: TStringField
      FieldName = 'CLASIF'
    end
  end
  object dsPlantillasCreditoCompras: TDataSource
    DataSet = qryPlantillasCreditoCompras
    Left = 572
    Top = 272
  end
  object dsPlantillasContadoCompras: TDataSource
    DataSet = qryPlantillasContadoCompras
    Left = 636
    Top = 272
  end
  object qryPlantillasContadoCompras: TFDQuery
    AutoCalcFields = False
    AfterOpen = qryPlantillasContadoComprasAfterOpen
    AfterInsert = qryPlantillasContadoComprasAfterInsert
    BeforeScroll = qryPlantillasContadoBeforeScroll
    IndexesActive = False
    Connection = Conexion
    SQL.Strings = (
      
        'select * from sic_plantillas_co_det where tipo = '#39'C'#39' and clasif=' +
        #39'Compra'#39
      'ORDER BY CREACION')
    Left = 636
    Top = 184
    object StringField9: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      FixedChar = True
      Size = 1
    end
    object StringField10: TStringField
      DisplayLabel = 'Cuenta'
      DisplayWidth = 24
      FieldName = 'CTA'
      Origin = 'CTA'
      Size = 50
    end
    object StringField11: TStringField
      DisplayLabel = 'Debe'
      DisplayWidth = 24
      FieldName = 'DEBE'
      Origin = 'DEBE'
      Size = 50
    end
    object StringField12: TStringField
      DisplayLabel = 'Haber'
      DisplayWidth = 24
      FieldName = 'HABER'
      Origin = 'HABER'
      Size = 50
    end
    object qryPlantillasContadoComprasFORMULA_DEBE: TStringField
      DisplayLabel = 'Formula Debe'
      DisplayWidth = 30
      FieldName = 'FORMULA_DEBE'
      Origin = 'FORMULA_DEBE'
      Size = 50
    end
    object qryPlantillasContadoComprasFORMULA_HABER: TStringField
      DisplayLabel = 'Formula Haber'
      DisplayWidth = 30
      FieldName = 'FORMULA_HABER'
      Origin = 'FORMULA_HABER'
      Size = 50
    end
    object SQLTimeStampField2: TSQLTimeStampField
      FieldName = 'CREACION'
      Origin = 'CREACION'
    end
    object qryPlantillasContadoComprasCLASIF: TStringField
      FieldName = 'CLASIF'
    end
  end
  object qryPlantillapagosaclientes: TFDQuery
    AutoCalcFields = False
    AfterOpen = qryPlantillapagosaclientesAfterOpen
    AfterInsert = qryPlantillapagosaclientesAfterInsert
    BeforeScroll = qryPlantillasContadoBeforeScroll
    IndexesActive = False
    Connection = Conexion
    SQL.Strings = (
      
        'select * from sic_plantillas_co_det where clasif='#39'Pago de client' +
        'e'#39
      'ORDER BY CREACION')
    Left = 372
    Top = 336
    object StringField13: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      FixedChar = True
      Size = 1
    end
    object StringField14: TStringField
      DisplayLabel = 'Cuenta'
      DisplayWidth = 24
      FieldName = 'CTA'
      Origin = 'CTA'
      Size = 50
    end
    object StringField15: TStringField
      DisplayLabel = 'Debe'
      DisplayWidth = 24
      FieldName = 'DEBE'
      Origin = 'DEBE'
      Size = 50
    end
    object StringField16: TStringField
      DisplayLabel = 'Haber'
      DisplayWidth = 24
      FieldName = 'HABER'
      Origin = 'HABER'
      Size = 50
    end
    object SQLTimeStampField3: TSQLTimeStampField
      FieldName = 'CREACION'
      Origin = 'CREACION'
    end
    object StringField17: TStringField
      FieldName = 'CLASIF'
    end
  end
  object dsPlantillapagosaclientes: TDataSource
    DataSet = qryPlantillapagosaclientes
    Left = 372
    Top = 424
  end
  object qryPlantillapagosaproveedores: TFDQuery
    AutoCalcFields = False
    AfterOpen = qryPlantillapagosaproveedoresAfterOpen
    AfterInsert = qryPlantillapagosaproveedoresAfterInsert
    BeforeScroll = qryPlantillasContadoBeforeScroll
    IndexesActive = False
    Connection = Conexion
    SQL.Strings = (
      
        'select * from sic_plantillas_co_det where clasif='#39'Pago a proveed' +
        'or'#39
      'ORDER BY CREACION')
    Left = 428
    Top = 336
    object StringField18: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      FixedChar = True
      Size = 1
    end
    object StringField19: TStringField
      DisplayLabel = 'Cuenta'
      DisplayWidth = 24
      FieldName = 'CTA'
      Origin = 'CTA'
      Size = 50
    end
    object StringField20: TStringField
      DisplayLabel = 'Debe'
      DisplayWidth = 24
      FieldName = 'DEBE'
      Origin = 'DEBE'
      Size = 50
    end
    object StringField21: TStringField
      DisplayLabel = 'Haber'
      DisplayWidth = 24
      FieldName = 'HABER'
      Origin = 'HABER'
      Size = 50
    end
    object SQLTimeStampField4: TSQLTimeStampField
      FieldName = 'CREACION'
      Origin = 'CREACION'
    end
    object StringField22: TStringField
      FieldName = 'CLASIF'
    end
  end
  object dsPlantillapagosaproveedores: TDataSource
    DataSet = qryPlantillapagosaproveedores
    Left = 428
    Top = 424
  end
  object qryPlantillanomina: TFDQuery
    AutoCalcFields = False
    AfterOpen = qryPlantillanominaAfterOpen
    AfterInsert = qryPlantillanominaAfterInsert
    BeforeScroll = qryPlantillasContadoBeforeScroll
    IndexesActive = False
    Connection = Conexion
    SQL.Strings = (
      'select * from sic_plantillas_co_det where clasif='#39'Nomina'#39
      'ORDER BY CREACION')
    Left = 492
    Top = 336
    object StringField23: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      FixedChar = True
      Size = 1
    end
    object StringField24: TStringField
      DisplayLabel = 'Cuenta'
      DisplayWidth = 24
      FieldName = 'CTA'
      Origin = 'CTA'
      Size = 50
    end
    object StringField25: TStringField
      DisplayLabel = 'Debe'
      DisplayWidth = 24
      FieldName = 'DEBE'
      Origin = 'DEBE'
      Size = 50
    end
    object StringField26: TStringField
      DisplayLabel = 'Haber'
      DisplayWidth = 24
      FieldName = 'HABER'
      Origin = 'HABER'
      Size = 50
    end
    object SQLTimeStampField5: TSQLTimeStampField
      FieldName = 'CREACION'
      Origin = 'CREACION'
    end
    object StringField27: TStringField
      FieldName = 'CLASIF'
    end
  end
  object dsPlantillanomina: TDataSource
    DataSet = qryPlantillanomina
    Left = 492
    Top = 424
  end
  object qrytiposPolizasPagosClientes: TFDQuery
    AfterInsert = qrytiposPolizasPagosClientesAfterInsert
    AfterScroll = qrytiposPolizasPagosClientesAfterScroll
    Connection = Conexion
    SQL.Strings = (
      'select * from tipos_polizas order by nombre')
    Left = 16
    Top = 384
  end
  object dstiposPolizasPagosClientes: TDataSource
    DataSet = qrytiposPolizasPagosClientes
    Left = 16
    Top = 432
  end
  object qryTiposPolizasPagosPro: TFDQuery
    AfterInsert = qryTiposPolizasPagosProAfterInsert
    AfterScroll = qryTiposPolizasPagosProAfterScroll
    Connection = Conexion
    SQL.Strings = (
      'select * from tipos_polizas order by nombre')
    Left = 56
    Top = 384
  end
  object dsTiposPolizasPagosPro: TDataSource
    DataSet = qryTiposPolizasPagosPro
    Left = 56
    Top = 432
  end
  object qryTiposPolizasNomina: TFDQuery
    AfterInsert = qryTiposPolizasNominaAfterInsert
    AfterScroll = qryTiposPolizasNominaAfterScroll
    Connection = Conexion
    SQL.Strings = (
      'select * from tipos_polizas order by nombre')
    Left = 96
    Top = 384
  end
  object dsTiposPolizasNomina: TDataSource
    DataSet = qryTiposPolizasNomina
    Left = 96
    Top = 432
  end
  object MainMenu1: TMainMenu
    Left = 180
    Top = 32
    object Configuracin1: TMenuItem
      Caption = 'Configuraci'#243'n'
      object ReestablecerPlantillas1: TMenuItem
        Caption = 'Reestablecer Plantillas'
        OnClick = ReestablecerPlantillas1Click
      end
    end
  end
  object qryPlantillaNCCCXC: TFDQuery
    AutoCalcFields = False
    AfterOpen = qryPlantillaNCCCXCAfterOpen
    AfterInsert = qryPlantillaNCCCXCAfterInsert
    BeforeScroll = qryPlantillasContadoBeforeScroll
    IndexesActive = False
    Connection = Conexion
    SQL.Strings = (
      
        'select * from sic_plantillas_co_det where clasif='#39'Nota de credit' +
        'o cxc'#39
      'ORDER BY CREACION')
    Left = 556
    Top = 336
    object StringField28: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      FixedChar = True
      Size = 1
    end
    object StringField29: TStringField
      DisplayLabel = 'Cuenta'
      DisplayWidth = 24
      FieldName = 'CTA'
      Origin = 'CTA'
      Size = 50
    end
    object StringField30: TStringField
      DisplayLabel = 'Debe'
      DisplayWidth = 24
      FieldName = 'DEBE'
      Origin = 'DEBE'
      Size = 50
    end
    object StringField31: TStringField
      DisplayLabel = 'Haber'
      DisplayWidth = 24
      FieldName = 'HABER'
      Origin = 'HABER'
      Size = 50
    end
    object SQLTimeStampField6: TSQLTimeStampField
      FieldName = 'CREACION'
      Origin = 'CREACION'
    end
    object StringField32: TStringField
      FieldName = 'CLASIF'
    end
  end
  object dsPlantillaNCCXC: TDataSource
    DataSet = qryPlantillaNCCCXC
    Left = 556
    Top = 424
  end
  object qryPlantillaNCCXP: TFDQuery
    AutoCalcFields = False
    AfterOpen = qryPlantillaNCCXPAfterOpen
    AfterInsert = qryPlantillaNCCXPAfterInsert
    BeforeScroll = qryPlantillasContadoBeforeScroll
    IndexesActive = False
    Connection = Conexion
    SQL.Strings = (
      
        'select * from sic_plantillas_co_det where clasif='#39'Nota de credit' +
        'o cxp'#39
      'ORDER BY CREACION')
    Left = 612
    Top = 336
    object StringField33: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      FixedChar = True
      Size = 1
    end
    object StringField34: TStringField
      DisplayLabel = 'Cuenta'
      DisplayWidth = 24
      FieldName = 'CTA'
      Origin = 'CTA'
      Size = 50
    end
    object StringField35: TStringField
      DisplayLabel = 'Debe'
      DisplayWidth = 24
      FieldName = 'DEBE'
      Origin = 'DEBE'
      Size = 50
    end
    object StringField36: TStringField
      DisplayLabel = 'Haber'
      DisplayWidth = 24
      FieldName = 'HABER'
      Origin = 'HABER'
      Size = 50
    end
    object SQLTimeStampField7: TSQLTimeStampField
      FieldName = 'CREACION'
      Origin = 'CREACION'
    end
    object StringField37: TStringField
      FieldName = 'CLASIF'
    end
  end
  object dsPlantillaNCCXP: TDataSource
    DataSet = qryPlantillaNCCXP
    Left = 612
    Top = 424
  end
  object qryTiposPolizasNCCXC: TFDQuery
    AfterInsert = qryTiposPolizasNominaAfterInsert
    AfterScroll = qryTiposPolizasNCCXCAfterScroll
    Connection = Conexion
    SQL.Strings = (
      'select * from tipos_polizas order by nombre')
    Left = 144
    Top = 384
  end
  object dsTiposPolizasNCCXC: TDataSource
    DataSet = qryTiposPolizasNCCXC
    Left = 144
    Top = 432
  end
  object qryTiposPolizasNCCXP: TFDQuery
    AfterInsert = qryTiposPolizasNominaAfterInsert
    AfterScroll = qryTiposPolizasNCCXPAfterScroll
    Connection = Conexion
    SQL.Strings = (
      'select * from tipos_polizas order by nombre')
    Left = 200
    Top = 384
  end
  object dsTiposPolizasNCCXP: TDataSource
    DataSet = qryTiposPolizasNCCXP
    Left = 200
    Top = 432
  end
end
