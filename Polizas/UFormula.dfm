object Formula: TFormula
  Left = 0
  Top = 0
  Caption = 'Formula'
  ClientHeight = 202
  ClientWidth = 330
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 157
    Width = 98
    Height = 13
    Caption = 'Buscar Cuentas (F4)'
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 183
    Width = 330
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object Memo1: TMemo
    Left = 8
    Top = 8
    Width = 313
    Height = 129
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
    OnKeyDown = Memo1KeyDown
  end
  object btnAceptar: TButton
    Left = 136
    Top = 152
    Width = 75
    Height = 25
    Caption = 'Aceptar'
    TabOrder = 2
    OnClick = btnAceptarClick
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'Password=masterkey'
      'User_Name=sysdba'
      'DriverID=FB')
    LoginPrompt = False
    Left = 483
    Top = 168
  end
end
